﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySupplyApp.Api.Models
{
    public class User
    {
        private string agreementnr;
        private string username;
        private string hashvalue;
        private string saltvalue;

        public User(string agreementnr, string username, string hashvalue, string saltvalue)
        {
            this.Agreementnr = agreementnr;
            this.Username = username;
            this.Hashvalue = hashvalue;
            this.Saltvalue = saltvalue;
        }

        public string Agreementnr
        {
            get { return agreementnr; }
            set { agreementnr = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; ; }
        }

        public string Hashvalue
        {
            get { return hashvalue; }
            set { hashvalue = value; ; }
        }

        public string Saltvalue
        {
            get { return saltvalue; }
            set { saltvalue = value; ; }
        }

    }
}
