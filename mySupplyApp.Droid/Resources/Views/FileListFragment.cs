﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

//using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using mySupplyApp.Droid.Resources.Views.adapters;
using MvvmCross.Binding.BindingContext;
using mySupplyApp.Core.ViewModels;
using mySupplyApp.Core.Models;
using MvvmCross.Droid.Support.V4;


namespace mySupplyApp.Droid.Resources.Views
{
    public class FileListFragment : ListFragment
    {
        public static readonly string DefaultInitialDirectory = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
        private FileListAdapter _adapter;
        private DirectoryInfo _directory;

        /*
        private byte[] _file;
        public byte[] _File
        {
            get => _file;
            set
            {
                _file = value;
            }
        }
        */
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _adapter = new FileListAdapter(Activity, new FileSystemInfo[0]);
            ListAdapter = _adapter;
            


          /*  var owner = this as IMvxBindingContextOwner;
            var set = owner.CreateBindingSet<IMvxBindingContextOwner, FileListViewModel>();
            set.Bind(this).For(_view => _view._File).To(_viewModel => _viewModel._File).TwoWay();
            set.Apply();
            */
        }
        /*
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.FileList, null);

            _adapter = new FileListAdapter(Activity, new FileSystemInfo[0]);
            ListAdapter = _adapter;
            return view;
        }
        */
        public override void OnResume()
        {
            base.OnResume();
            RefreshFilesList(DefaultInitialDirectory);
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var fileSystemInfo = _adapter.GetItem(position);

            //if the file exists, do x
            if (fileSystemInfo.IsFile())
            {
                if (fileSystemInfo.Extension.Equals(".pdf"))
                {
                    Toast.MakeText(Activity, "You selected file " + fileSystemInfo.FullName + ". Filesize =" + fileSystemInfo.Attributes, ToastLength.Short).Show();
                    //upload
                    byte[] bytes = System.IO.File.ReadAllBytes(fileSystemInfo.FullName);
                   
                    this.Activity.SetResult(Android.App.Result.Ok);
                    this.Activity.Finish();

                }
                else
                {
                    Toast.MakeText(Activity, "You have selected an invalid file type. You can only upload PDF files", ToastLength.Long).Show();
                }
            }
            //if its not a file, 
            else
            {
                RefreshFilesList(fileSystemInfo.FullName);
            }

            base.OnListItemClick(l, v, position, id);
        }

        public void RefreshFilesList(string directory)
        {
            IList<FileSystemInfo> files = new List<FileSystemInfo>();
            var dir = new DirectoryInfo(directory);


            try
            {
                //For every item in the directory, add it to the list that will be added later to the adapter
                foreach (var item in dir.GetFileSystemInfos().Where(item => item.IsVisible()))
                {
                    files.Add(item);
                }
            }
            catch(Exception e)
            {
                //Log and toast for debugging purposes
                Log.Error("FileListFragment", "Couldnt access Dir " + dir.FullName + "; " + e);
                Toast.MakeText(Activity, "Problem retrieving contents of " + directory, ToastLength.Long).Show();
                return;
            }

            _directory = dir;
            //clear the adapter of items
            _adapter.Clear();
            //add the list from the foreach to the adapter
            _adapter.AddDirectoryContents(files);

            //To force the ListView to update itself when the data in the adapter changes
            ListView.RefreshDrawableState();

            //Logging to see the actual contents for development purposes only
            Log.Verbose("FileListFragment", "Displaying the contents of dir " + directory);
        }

        //Use this if the <fragment class> doesnt work in the layout file
        /* public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
         {
             // Use this to return your custom view for this Fragment
             // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

             return base.OnCreateView(inflater, container, savedInstanceState);
         }*/
    }
}