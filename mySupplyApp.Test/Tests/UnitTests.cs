﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mySupplyApp.Core.ViewModels;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;
using mySupplyApp.Api.Services;

namespace mySupplyApp.Test
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        [DataRow("0001", "test", "test")]
        public void LoginPositive(string agreementnr, string username, string password)
        {
            LoginViewModel vm = new LoginViewModel(null);

            vm.Model.Agreementnr = agreementnr;
            vm.Model.Username = username;
            vm.Model.Password = password;
            vm.Login();

            Assert.AreNotEqual(null, UserModel.LoggedinUser);
            Assert.AreEqual(agreementnr, UserModel.LoggedinUser.Agreementnr);
            Assert.AreEqual(username, UserModel.LoggedinUser.Username);
            Assert.AreEqual(password, UserModel.LoggedinUser.Password);
        }

        
    }
}
