﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace mySupplyApp.Droid.Resources.Views.adapters
{
    public class FileListAdapter : ArrayAdapter<FileSystemInfo>
    {
        private readonly Context context;
        public FileListAdapter(Context context, IList<FileSystemInfo> fileSystemInfoList) 
            : base (context, Resource.Layout.filelistitem, Android.Resource.Id.Text1, fileSystemInfoList)
        {
            this.context = context;
        }

        public void AddDirectoryContents(IEnumerable<FileSystemInfo> dirContents)
        {
            Clear();

            if (dirContents.Any())
            {
                AddAll(dirContents.ToArray());
                NotifyDataSetChanged();
            }
            else
            {
                NotifyDataSetInvalidated();
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var fileSystemEntry = GetItem(position);
            FileListRowViewHolder viewHolder;
            View view;

            if(convertView == null)
            {
                var inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);
                view = inflater.Inflate(Resource.Layout.filelistitem, parent, false);
                viewHolder = new FileListRowViewHolder(view.FindViewById<TextView>(Resource.Id.file_picker_text), 
                    view.FindViewById<ImageView>(Resource.Id.file_picker_image));
                view.Tag = viewHolder;
            }
            else
            {
                view = convertView;
                viewHolder = (FileListRowViewHolder)view.Tag;
            }
            viewHolder.Update(fileSystemEntry.Name, fileSystemEntry.IsDirectory() ? Resource.Drawable.folder : Resource.Drawable.file);
            return view;
            //return base.GetView(position, convertView, parent);
        }
    }
}