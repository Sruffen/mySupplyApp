﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using mySupplyApp.Core.Models;
using Newtonsoft.Json;

namespace mySupplyApp.Core.Services
{
    public class API_Service
    {
        private const string endpoint = "https://MySupply0916.azurewebsites.net/api";
        private const string API_KEY = "AFA27ASD";

        private async Task<string> CallGet(Uri URL, UserModel user = null)
        {
            string jsonResponse = null;

            HttpClient client = new HttpClient();

            // Use default if none were assigned.
            if (user == null)
                user = UserModel.LoggedinUser;

            // check to make sure its not null.
            if (user == null)
                throw new ArgumentNullException("user", "No user was assigned and the default user was null");

            // Hash username and password
            string hashString = new Security().HashString(user.Username + user.Password);

            //and add as basic authentication
            var authData = string.Format("{0}:{1}:{2}", user.Username, hashString, API_KEY);
            string authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            
            HttpResponseMessage response = await client.GetAsync(URL);
            HttpContent content = response.Content;
            jsonResponse = await content.ReadAsStringAsync();
            
            return jsonResponse;
        }

        private async Task<string> CallPost(Uri URL, byte[] bytes, UserModel user = null)
        {
            string jsonResponse = null;

            HttpClient client = new HttpClient();

            // Use default if none were assigned.
            if (user == null)
                user = UserModel.LoggedinUser;

            // check to make sure its not null.
            if (user == null)
                throw new ArgumentNullException("user", "No user was assigned and the default user was null");

            // Hash username and password
            string hashString = new Security().HashString(user.Username + user.Password);

            //and add as basic authentication
            var authData = string.Format("{0}:{1}:{2}", user.Username, hashString, API_KEY);
            string authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            ByteArrayContent byteArrayContent = new ByteArrayContent(bytes);
            HttpResponseMessage response = await client.PostAsync(URL, byteArrayContent);
            HttpContent _content = response.Content;
            jsonResponse = await _content.ReadAsStringAsync();
            
            return jsonResponse;
        }
            
        public async Task<UserModel> LoginAsync(string agreementNr, string username, string password)
        {
            UserModel result = null, user;
            API_Service api = new API_Service();

            user = new UserModel() { Agreementnr = agreementNr, Username = username, Password = password };

            Uri url = new Uri(endpoint + "/Login?agreementnr=" + agreementNr + "&username=" + username);


            /* Do the call and get a response: */
            string json = await api.CallGet(url, user);
            result = JsonConvert.DeserializeObject<UserModel>(json);
            result.Password = user.Password;
            return result;
        }

        public async Task<bool> UploadAsync(byte[] bytes, string filename)
        {
            API_Service api = new API_Service();

            Uri url = new Uri(endpoint + "/Document/Upload/" + filename +  "?username=" + UserModel.LoggedinUser.Username);
            string json = await api.CallPost(url, bytes);
            var js = JsonConvert.DeserializeObject(json);

            return (bool)js;
        }

        public async Task<byte[]> GetDataunitAsync(int id)
        {
            API_Service api = new API_Service();

            Uri url = new Uri(endpoint + "/Document/Data/" + id + "?username=" + UserModel.LoggedinUser.Username);
            string json = await api.CallGet(url);
            var js = JsonConvert.DeserializeObject<byte[]>(json);

            return (byte[])js;
        }

        public async Task<List<DocumentModel>> GetDocuments()
        {
            API_Service api = new API_Service();

            Uri url = new Uri(endpoint + "/Document/?username=" + UserModel.LoggedinUser.Username);
            string json = await api.CallGet(url);
            var js = JsonConvert.DeserializeObject<List<DocumentModel>>(json);

            //return (List<DocumentModel>)js;

            return ListFiller(js);
        }

        private List<DocumentModel> ListFiller(List<DocumentModel> toFill)
        {
            List<DocumentModel> fillers = getDocumentsTest();

            for (int i = 0, j= 0; i < toFill.Count; i++, j++)
            {
                if (j >= 5)
                    j = 0;

                toFill[i].Created           = fillers[j].Created;
                toFill[i].Recipent          = fillers[j].Recipent;
                toFill[i].Destination       = fillers[j].Destination;
                toFill[i].Out               = fillers[j].Out;
                toFill[i].In                = fillers[j].In;
                toFill[i].DocumentHistories = fillers[j].DocumentHistories;
                toFill[i].MetaData          = fillers[j].MetaData;
                toFill[i].User              = fillers[j].User;
            }

            return toFill;
        }

        public List<DocumentModel> getDocumentsTest()
        {
            //Pre objects for document
            List<DocumentHistory> documentHistories = new List<DocumentHistory>();
            documentHistories.Add(new DocumentHistory
            {
                Timestamp = DateTime.Now,
                Headline = "daniel sutter",
                Description = "not true btw"
            });
            documentHistories.Add(new DocumentHistory
            {
                Timestamp = DateTime.Now,
                Headline = "daniel sutter1",
                Description = "not true btw"
            });
            documentHistories.Add(new DocumentHistory
            {
                Timestamp = DateTime.Now,
                Headline = "daniel sutter2",
                Description = "not true btw"
            });
            documentHistories.Add(new DocumentHistory
            {
                Timestamp = DateTime.Now,
                Headline = "daniel sutter3",
                Description = "not true btw"
            });
            ///////////////////
            Dictionary<string, string> metaData = new Dictionary<string, string>();
            metaData.Add("Owner1", "daniel");
            metaData.Add("Owner2", "daniel2");
            metaData.Add("Owner3", "daniel3");
            metaData.Add("Owner4", "daniel4");
            metaData.Add("Owner5", "daniel5");
            UserModel user = new UserModel
            {
                Username = "AndersGertsen",
                Agreementnr = "9852",
                Password = "XXXXXX"
            };
            //Document data to fill out list
            List<DocumentModel> documents = new List<DocumentModel>();
            documents.Add(new DocumentModel {
                Name = "faktura1",
                Created = DateTime.Now,
                Recipent = "firma",
                Destination = "future",
                Out = false,
                In = true,
                DocumentHistories = documentHistories,
                MetaData = metaData,
                User = user
            });
            documents.Add(new DocumentModel
            {
                Name = "faktura2",
                Created = DateTime.Now,
                Recipent = "firma2",
                Destination = "future2",
                Out = true,
                In = false,
                DocumentHistories = documentHistories,
                MetaData = metaData,
                User = user
            });
            documents.Add(new DocumentModel
            {
                Name = "faktura3",
                Created = DateTime.Now,
                Recipent = "firma3",
                Destination = "future3",
                Out = true,
                In = true,
                DocumentHistories = documentHistories,
                User = user
            });
            documents.Add(new DocumentModel
            {
                Name = "faktura4",
                Created = DateTime.Now,
                Recipent = "firma4",
                Destination = "future4",
                Out = false,
                In = false,
                MetaData = metaData,
                User = user
            });
            documents.Add(new DocumentModel
            {
                Name = "faktura5",
                Created = DateTime.Now,
                Recipent = "firma5",
                Destination = "future5",
                Out = false,
                In = true,
                DocumentHistories = documentHistories,
                MetaData = metaData,
                User = user
            });
            return documents;
        }
    }
}
