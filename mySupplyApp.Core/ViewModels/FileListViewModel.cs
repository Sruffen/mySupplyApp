﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;
using System.IO;
using mySupplyApp.Core;

namespace mySupplyApp.Core.ViewModels
{
    public class FileListViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService navigationService;

        // Name, path
        private static Dictionary<string, string> fileList;

        public List<string> FileList
        {
            get { return fileList.Keys.ToList(); }
        }

        public void AddFileToList (string name, string path) 
        {
            fileList.Add(name, path);
            RaisePropertyChanged("FileList");
        }

        public FileListViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            fileList = new Dictionary<string, string>();

            RefreshFilesList(Helpers.DefaultPath);

        }

        public void RefreshFilesList(string directory)
        {
            fileList.Clear();
            var dir = new DirectoryInfo(directory);
            try
            {
                //For every item in the directory, add it to the list that will be added later to the adapter
                foreach (var item in dir.GetFileSystemInfos().Where(item => item.IsVisible()))
                {
                    if (".pdf".Equals(item.Extension.ToLower()) || Helpers.IsDirectory(item))
                    {
                        AddFileToList(item.Name, item.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                return;
            }
        }

        public IMvxCommand ItemClickCommand => new MvxCommand<string>(ItemClick);

        public async void ItemClick(string itemName)
        {
            string path;

            if (!fileList.TryGetValue(itemName, out path))
            {
                throw new Exception();
            }

            if(File.Exists(path))
            {
                byte[] bytes = System.IO.File.ReadAllBytes(path);

                bool success = await Upload(bytes, Path.GetFileNameWithoutExtension(path));

                if (success)
                {
                    //do success stuff
                    await navigationService.Close(this);
                }
                else
                {
                    // do fail stuff
                }

            }
            else if (Directory.Exists(path))
            {
                RefreshFilesList(path);
            }
            else
            {
                throw new Exception();
            }
        }

        public async Task<bool> Upload(byte[] bytes, string filename)
        {
            bool result;

            result = await new API_Service().UploadAsync(bytes, filename);

            return result;
        }
    }
}
