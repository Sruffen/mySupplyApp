﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using mySupplyApp.Api.Models;
using mySupplyApp.Core.Models;

namespace mySupplyApp.Api.Services
{
    public class DataAcess
    {

        /// <summary>
        /// Returns a boolean describing if there is access to the database.
        /// </summary>
        /// <returns>boolean</returns>
        public bool HasAccess()
        {
            bool result = false;
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("SELECT @param1");
            command.CommandText = sqlQuery;

            // Add parameters here
            command.Parameters.Add(new SqlParameter("param1", 1));

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.Connection = connection;
                    connection.Open();

                    command.ExecuteScalarAsync();

                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            

            return result;
        }

        public byte[] GetDataunitByID(int id, string username)
        {
            byte[] result = null;
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("SELECT * FROM dataunits WHERE documentID = @docid");

            // Add parameters here
            command.Parameters.Add(new SqlParameter("docid", id));

            // Add condtitional info here:

            // Try to execute sql query
            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.CommandText = sqlQuery;
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //reader.GetBytes(reader.GetOrdinal("bytes"), 0, result, 0, Int32.MaxValue);
                            var a = (reader["bytes"]);
                            result = (byte[])(reader["bytes"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result = null;
            }

            return result;
        }

        public IEnumerable<DocumentModel> GetAllDocumentsForUsername(string username)
        {
            List<DocumentModel> result = new List<DocumentModel>();
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("SELECT * FROM documents ");

            // Add parameters here

            // Add condtitional info here:

            // Try to execute sql query
            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.CommandText = sqlQuery;
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DocumentModel doc = new DocumentModel();
                            doc.id = reader.GetInt32(reader.GetOrdinal("id"));
                            doc.Name = reader.GetString(reader.GetOrdinal("name"));
                            result.Add(doc);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result = null;
            }

            return result;
        }

        public bool ValidateAPIkey(string apikey)
        {
            bool result = false;
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("" +
                "SELECT 1 FROM apikeys " +
                    "WHERE [key] = @key " +
                    "AND active = 1");
            // The [] brackets escapes the sql keyword 'key'

            command.CommandText = sqlQuery;

            // Add parameters here
            command.Parameters.Add(new SqlParameter("key", apikey));

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        result = reader.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }
            
            return result;
        }

        public User GetUserByUsername(string username)
        {
            User result = null;
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("" +
                "SELECT * FROM users " +
                    "WHERE username = @username ");

            // Add parameters here
            command.Parameters.Add(new SqlParameter("username", username));

            // Add condtitional info here

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.CommandText = sqlQuery;
                    command.Connection = connection;
                    connection.Open();

                    username = string.Empty;
                    string agreementnr = string.Empty, hashvalue = string.Empty, saltvalue = string.Empty;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            agreementnr = reader.GetString(reader.GetOrdinal("agreementnumber"));
                            username = reader.GetString(reader.GetOrdinal("username"));
                            hashvalue = reader.GetString(reader.GetOrdinal("hashvalue"));
                            saltvalue = reader.GetString(reader.GetOrdinal("saltvalue"));

                            result = new User(agreementnr, username, hashvalue, saltvalue);
                        }
                    }
                }
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }

        public bool UploadDocument(byte[] bytes, DocumentModel doc, User user)
        {
            bool result = false;
            int documentId = 0, returnId = 0;
            string sqlQuery = "";
            SqlCommand commandPrimus = new SqlCommand();
            SqlCommand commandSecundus = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("INSERT INTO documents OUTPUT INSERTED.ID VALUES(@docname)");
            commandPrimus.CommandText = sqlQuery;

            sqlQuery = string.Format("INSERT INTO dataunits OUTPUT INSERTED.ID VALUES(@bytes, @docID)");
            commandSecundus.CommandText = sqlQuery;

            // Add parameters here
            commandPrimus.Parameters.Add(new SqlParameter("docname", doc.Name));

            commandSecundus.Parameters.Add(new SqlParameter("bytes", bytes));

            // Add condtitional info here:

            // Try to execute sql query
            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {

                    commandPrimus.Connection = connection;
                    commandSecundus.Connection = connection;
                    connection.Open();

                    // Excute fir command
                    documentId = (int)commandPrimus.ExecuteScalar();

                    // use the id from the first as a parameter in the second command
                    commandSecundus.Parameters.Add(new SqlParameter("docID", documentId));

                    // exeute the second command.
                    returnId = (int)commandSecundus.ExecuteScalar();

                    // use the returned id's of both commands to make sure it was a success
                    if (returnId > 0 && documentId > 0)
                        result = true;
                }
            }
            catch (Exception e)
            {
                result = false;
            }

            return result;
        }
    }
    

    /* Template */
    /*
     public T TemplateName()
        {
            T result = default(T);
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("");

            // Add parameters here

            // Add condtitional info here:

            // Try to execute sql query
            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.CommandText = sqlQuery;
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        
                    }
                }
            }
            catch (Exception)
            {
                result = default(T);
            }

            return result;
        }
        */
}
 