﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySupplyApp.Core.Models
{
    public class WrongCredentialsDialog
    {
        public Action<bool> OkayResetPasswordCallBack { get; set; }
        public string DialogText { get; set; }
    }
}
