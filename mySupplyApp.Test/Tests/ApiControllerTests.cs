﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mySupplyApp.Api.Controllers;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;

namespace mySupplyApp.Test.Tests
{
    
    /*
    * https://docs.microsoft.com/en-us/aspnet/web-api/overview/testing-and-debugging/unit-testing-controllers-in-web-api 
    */

    [TestClass]
    public class LoginControllerTests
    {
        [TestMethod]
        [DataRow("9999", "Test")]
        public void LoginGet(string agreementnr, string username)
        {
            // arrange
            LoginController controller = new LoginController();

            // act
            IHttpActionResult actionResult =  controller.Get(agreementnr, username);
            OkNegotiatedContentResult<UserModel> contentResult = actionResult as OkNegotiatedContentResult<UserModel>;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(username, contentResult.Content.Username);
        }

        [TestMethod]
        public void UploadTest()
        {
            // Arrange
            API_Service api = new API_Service();
            UserModel.LoggedinUser = new UserModel() { Username = "Test"};
            bool result = false;

            // Act
            try
            {
               result = api.UploadAsync(new byte[10]).Result;
            
            }
            catch (Exception e)
            {
                Assert.Fail();
                result = false;
            }

            // Assert
            Assert.IsTrue(result);

        }
    }
    
}
