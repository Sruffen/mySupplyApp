﻿using System;
using mySupplyApp.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mySupplyApp.Api.Services;
using mySupplyApp.Api.Models;
using mySupplyApp.Api.Filters;
using mySupplyApp.Core.Models;

namespace mySupplyApp.Test
{
    [TestClass]
    public class DataAcessTests
    {
        /// <summary>
        /// If this method fails all other tests in this class will fail too
        /// </summary>
        [TestMethod]
        public void TestAcess()
        {
            DataAcess db = new DataAcess();

            Assert.IsTrue(db.HasAccess(), "If this method fails all other tests in this class will fail too");
        }

        [TestMethod]
        [DataRow("AFA27ASD")]
        public void ValidateAPIkeyPositive(string apikey)
        {
            DataAcess db = new DataAcess();
            bool result = false;

            try
            {
                result = db.ValidateAPIkey(apikey);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            Assert.IsTrue(result, apikey);
        }

        [TestMethod]
        [DataRow("")]
        [DataRow("SHOULDFAILBUTDOESIT?")]
        [DataRow("TESTKEY")] // This exists in the database, but it is inactive and thus should fail.
        public void ValidateAPIkeyNegative(string apikey)
        {
            DataAcess db = new DataAcess();
            bool result = false;

            try
            {
                result = db.ValidateAPIkey(apikey);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            Assert.IsFalse(result, apikey);
        }

        [TestMethod]
        [DataRow("9999", "Test", "db0978e8d4ba51b12f9c7b137e87d9a03ba358c2f69fa6a9c8516f76a9cf195b", "d7eb98db1be2960fb2ada6b3b5c4876f957d73041fc5dff845534e258f10947f")]
        public void FindUserByUsernamePositive(string agreementnr, string username, string hashvalue, string saltvalue)
        {
            DataAcess db = new DataAcess();
            User expected = new User(agreementnr, username, hashvalue, saltvalue);
            User actual = null;

            try
            {
                actual = db.GetUserByUsername(username);
            }
            catch (Exception)
            {
                Assert.Fail("Username was: " + username);
            }

            Assert.IsNotNull(actual, "Username was: " + username);
            Assert.AreEqual(expected.Agreementnr, actual.Agreementnr, "Expected: " + expected.Agreementnr + " | Actual: " + actual.Agreementnr);
            Assert.AreEqual(expected.Username,    actual.Username,    "Expected: " + expected.Username    + " | Actual: " + actual.Username);
            Assert.AreEqual(expected.Hashvalue,   actual.Hashvalue,   "Expected: " + expected.Hashvalue   + " | Actual: " + actual.Hashvalue);
            Assert.AreEqual(expected.Saltvalue,   actual.Saltvalue,   "Expected: " + expected.Saltvalue   + " | Actual: " + actual.Saltvalue);
        }

        [TestMethod]
        [DataRow("9999",  "",        "db0978e8d4ba51b12f9c7b137e87d9a03ba358c2f69fa6a9c8516f76a9cf195b", "d7eb98db1be2960fb2ada6b3b5c4876f957d73041fc5dff845534e258f10947f")]
        [DataRow("9999",  "    ",    "db0978e8d4ba51b12f9c7b137e87d9a03ba358c2f69fa6a9c8516f76a9cf195b", "d7eb98db1be2960fb2ada6b3b5c4876f957d73041fc5dff845534e258f10947f")]
        [DataRow("9999",  null,      "db0978e8d4ba51b12f9c7b137e87d9a03ba358c2f69fa6a9c8516f76a9cf195b", "d7eb98db1be2960fb2ada6b3b5c4876f957d73041fc5dff845534e258f10947f")]
        [DataRow("9999",  "wrong",   "db0978e8d4ba51b12f9c7b137e87d9a03ba358c2f69fa6a9c8516f76a9cf195b", "d7eb98db1be2960fb2ada6b3b5c4876f957d73041fc5dff845534e258f10947f")]
        public void FindUserByUsernameNegative(string agreementnr, string username, string hashvalue, string saltvalue)
        {
            DataAcess db = new DataAcess();
            User expected = new User(agreementnr, username, hashvalue, saltvalue);
            User actual = null;

            try
            {
                actual = db.GetUserByUsername(username);
            }
            catch (Exception)
            {
                Assert.Fail("Username was: " + username);
            }

            Assert.IsNull(actual, "Username was: " + username);
        }

        [TestMethod]
        public void UploadDocumentPositive()
        {
            DataAcess db = new DataAcess();
            DocumentModel doc = new DocumentModel();
            byte[] b = new byte[] { 000 };

            Api.Models.User user = new Api.Models.User("9999", "Test", "", "");
            doc.Name = user.Username + " " + user.Agreementnr + " " + DateTime.Now;



            bool result = db.UploadDocument(b, doc, user);

            Assert.IsTrue(result);
        }
    }
}
