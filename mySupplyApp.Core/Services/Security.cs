﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace mySupplyApp.Core.Services
{
    public class Security
    {
        /// <summary>
        /// Hashes a string value using SHA256 hashing.
        /// </summary>
        /// <param name="value">string to be hashed</param>
        /// <returns>hash value as a string</returns>
        public string HashString(string value)
        {
            string result = string.Empty;

            byte[] bytes = Encoding.UTF8.GetBytes(value);
            
            byte[] hash = new SHA256Managed().ComputeHash(bytes);

            foreach (byte x in hash)
            {
                result += String.Format("{0:x2}", x);
            }

            return result;
        }

        /// <summary>
        /// Generates a random salt value.
        /// </summary>
        /// <returns>salt value as a string</returns>
        public string GetSaltValue()
        {
            string saltString = string.Empty;
            byte[] salt;

            new RNGCryptoServiceProvider().GetBytes(salt = new byte[32]);
            
            foreach (byte x in salt)
            {
                saltString += string.Format("{0:x2}", x);
            }

            return saltString;
        }
    }
}
