﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using mySupplyApp.Api.Filters;
using mySupplyApp.Api.Models;
using mySupplyApp.Api.Services;
using mySupplyApp.Core.Models;

namespace mySupplyApp.Api.Controllers
{
    [BasicAuthentication]
    public class LoginController : ApiController
    {
        // GET: api/Login/
        public IHttpActionResult Get(string agreementnr, string username)
        {
            User user = null;
            if (!string.IsNullOrWhiteSpace(agreementnr) && !string.IsNullOrWhiteSpace(username))
            {
                DataAcess db = new DataAcess();
                user = db.GetUserByUsername(username);

                if (user != null && agreementnr.Equals(user.Agreementnr))
                {
                    UserModel result = new UserModel() {Username = user.Username, Agreementnr = user.Agreementnr, Password = string.Empty };
                    return Ok(result);
                }
            }

            return BadRequest("Headers missing or invalid");
        }
    }
}
