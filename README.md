Users that can be used to login. All users have the same functionality.

Name and password is case sensitive.

Agreementnr     Name    Password

* 9999          Test    test
* 0000            Admin   1234
* 1000            Daniel  dgl
* 0200            Tim     tm
* 0030            Anders  agbn



These user can also be found in mySupplyApp.CommandConsole -> program.cs