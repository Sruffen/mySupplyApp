﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;

namespace mySupplyApp.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService navigationService;

        private UserModel model;

        public UserModel Model
        {
            get { return model; }
            set { SetProperty<UserModel>(ref model, value); }
        }


        public IMvxCommand LoginCommand => new MvxCommand(Login);

        private MvxInteraction<string> _interaction = new MvxInteraction<string>();
        public IMvxInteraction<string> Interaction => _interaction;
        
        public LoginViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.Model = new UserModel();
        }
        
        public async void Login()
        {
            UserModel currentUser = await new API_Service().LoginAsync(Model.Agreementnr, Model.Username, Model.Password);
            
            if (currentUser.Username != null)
            {
                UserModel.LoggedinUser = currentUser;

                await this.navigationService.Navigate<DocumentOverviewViewModel>();
            }
            else
            {
                var request = "Wrong credentials";
                _interaction.Raise(request);
            }
        }

    }
}
