﻿using mySupplyApp.Api.Filters;
using mySupplyApp.Api.Models;
using mySupplyApp.Api.Services;
using mySupplyApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;

namespace mySupplyApp.Api.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/Document")]
    public class DocumentController : ApiController
    {
        // GET: api/Document
        [HttpGet]
        public async Task<IHttpActionResult> Get(string username)
        {
            IEnumerable<DocumentModel> result = null;
            DataAcess db = new DataAcess();

            result = db.GetAllDocumentsForUsername(username);

            return Ok(result);
        }

        // POST: api/Document
        [Route("Upload/{filename}")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(string filename, string username)
        {
            bool result = false;
            byte[] bytes;

            try
            {
                bytes = await Request.Content.ReadAsByteArrayAsync();
            }
            catch (Exception e)
            {
                throw;
            }

            DataAcess db = new DataAcess();
            DocumentModel doc;
            User user;

            user = db.GetUserByUsername(username);
            doc = new DocumentModel();

            doc.Name = filename + "-" + user.Username + "-" + DateTime.Now.ToFileTimeUtc();

            result = db.UploadDocument(bytes, doc, user);

            return Ok(result);
        }

        [Route("Data/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id, string username)
        {
            byte[] bytes;
            DataAcess db = new DataAcess();

            User user;

            user = db.GetUserByUsername(username);

            try
            {
                bytes = db.GetDataunitByID(id, username);
            }
            catch (Exception e)
            {
                bytes = null;
            }

            return Ok(bytes);
        }
    }
}
