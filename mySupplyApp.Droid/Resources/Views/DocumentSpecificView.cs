﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Platforms.Android.WeakSubscription;
using mySupplyApp.Core.ViewModels;
using mySupplyApp.Droid.Resources.Views.fragments;


namespace mySupplyApp.Droid.Resources.Views
{
    [Activity(Label = "DocumentSpecificView", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DocumentSpecificView : MvxTabsFragmentActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Core.Helpers.WritePath = Android.OS.Environment.DirectoryDownloads;

            mySupplyApp.Core.Helpers.OpenDocMethod = ViewDocument;

            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.WriteExternalStorage }, 0);
            }

            //subscription = ViewModel.WeakSubscribe()

        }

        public void ViewDocument(string path)
        {

            Java.IO.File file = new Java.IO.File(path);
            string application = "application/pdf";

            View(this, path, application);
        }

        public DocumentSpecificViewModel documentSpecificViewModel
        {
            get { return (DocumentSpecificViewModel)base.ViewModel; }
        }

        protected override void AddTabs(Bundle args)
        {
            AddTab<DetailsFragment>("Details", "Detaljer", args, documentSpecificViewModel.detailsTab);
            AddTab<HistoryFragment>("History", "Historik", args, documentSpecificViewModel.historyTab);
            AddTab<ValidationFragment>("Validation", "Validering", args, documentSpecificViewModel.validationTab);
            AddTab<AnnexFragment>("Annex", "Bilag", args, documentSpecificViewModel.annexTab);
        }

        public DocumentSpecificView()
            : base(Resource.Layout.DocumentSpecific, Resource.Id.actualtabcontent)
        {

        }

        public void OpenFile()
        {

            string application = "";
        }

        public Android.Net.Uri WrapFileWithUri(Context context, Java.IO.File file)
        {
            Android.Net.Uri result;
            if (Build.VERSION.SdkInt < (BuildVersionCodes)24)
            {
                result = Android.Net.Uri.FromFile(file);
            }
            else
            {
                result = FileProvider.GetUriForFile(context, context.ApplicationContext.PackageName + ".provider", file);
            }
            return result;
        }

        public void View(Context context, string path, string mimeType)
        {
            Intent viewIntent = new Intent(Intent.ActionView);
            Java.IO.File document = new Java.IO.File(path);
            viewIntent.SetDataAndType(WrapFileWithUri(context, document), mimeType);
            viewIntent.SetFlags(ActivityFlags.NewTask);
            viewIntent.AddFlags(ActivityFlags.GrantReadUriPermission);
            context.StartActivity(Intent.CreateChooser(viewIntent, "your text"));
        }
    }
}
