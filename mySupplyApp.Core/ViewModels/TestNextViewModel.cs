﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySupplyApp.Core.ViewModels
{
    public class TestNextViewModel : MvxViewModel<TestTempModel>
    {
        private readonly IMvxNavigationService navigationService;

        public TestTempModel Model { get; private set; }

        public string Time
        {
            get { return "Recorded at: " + Model.TimeRecorded.ToLongTimeString(); }
        }

        public TestNextViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        public override void Prepare(TestTempModel parameter)
        {
            Model = parameter;
            Model.TimeRecorded = DateTime.Now;
        }

        public IMvxCommand AddToListCommand => new MvxCommand(AddToList);
        public IMvxCommand PreviousPageCommand => new MvxCommand(PreviousPage);

        public async void AddToList()
        {
            await this.navigationService.Navigate<TestListViewModel, TestTempModel>(Model);
        }

        public async void PreviousPage()
        {
            await this.navigationService.Close(this);
        }

    }
}
