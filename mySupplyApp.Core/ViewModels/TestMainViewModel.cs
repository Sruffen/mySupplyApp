﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;

namespace mySupplyApp.Core.ViewModels
{
    public class TestMainViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService navigationService;

        private TestTempModel model;

        //TODO: DELET THIS
        //private UserService userService = new UserService();

        public TestTempModel Model
        {
            get { return model; }
            set
            {
                model = value;
                RaisePropertyChanged();
            }
        }

        public async void NextPage()
        {
            bool success = await navigationService.Navigate<TestNextViewModel, TestTempModel>(Model);

            if (success)
            {
                Model = new TestTempModel();
                
            }
        }

        public TestMainViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            Model = new TestTempModel();
        }

        public override void Prepare()
        {
            base.Prepare();
        }

        /** Commands **/
        public IMvxAsyncCommand IncreaseNumberCommand => new MvxAsyncCommand(IncreaseNumberAsync);
        public IMvxCommand DecreaseNumberCommand => new MvxCommand(DecreaseNumber);
        public IMvxCommand NextPageCommand => new MvxCommand(NextPage);
   
        
        private async Task IncreaseNumberAsync()
        {
            //Model.Counter++;
            //string s = await userService.testApiAsync();
            //Model.Name = s;
        }

        private void DecreaseNumber()
        {
            Model.Counter--;
        }
    }
}
