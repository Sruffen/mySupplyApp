﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Support.V4.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using Xamarin.Essentials;

namespace mySupplyApp.Droid.Resources.Views
{
    [Activity(Label = "FileListView", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class FileListView : MvxActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.FileList);
            Platform.Init(this, savedInstanceState);

            var current = Connectivity.NetworkAccess;
            var profiles = Connectivity.ConnectionProfiles;

            if (current == NetworkAccess.Internet)
            {
                if (profiles.Contains(ConnectionProfile.WiFi))
                {
                    Toast.MakeText(this, "Du er koblet til en WiFi forbindelse. Funktionerne på denne skærm kan være usikre, afhængig af det WiFi du bruger." , ToastLength.Long).Show();
                }

                if (profiles.Contains(ConnectionProfile.Cellular))
                {
                    Toast.MakeText(this, "Du bruger en cellular forbindelse. Funktionerne på denne skærm kan anvende meget data.", ToastLength.Long).Show();
                }
            }

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}