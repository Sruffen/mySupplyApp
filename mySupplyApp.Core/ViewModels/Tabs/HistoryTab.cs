﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;

namespace mySupplyApp.Core.ViewModels.Tabs
{
    public class HistoryTab : MvxViewModel<List<DocumentHistory>>
    {

        private List<DocumentHistory> documentHistories;

        public List<DocumentHistory> DocumentHistories
        {
            get { return documentHistories; }
            set { documentHistories = value; }
        }

        public override void Prepare(List<DocumentHistory> histories)
        {
            DocumentHistories = histories;
        }
    }
}
