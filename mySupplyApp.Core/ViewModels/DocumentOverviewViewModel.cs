﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;

namespace mySupplyApp.Core.ViewModels
{
    public class DocumentOverviewViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService navigationService;
        private readonly API_Service api = new API_Service();

        private List<DocumentModel> documents;

        public List<DocumentModel> Documents
        {
            get { return documents; }
            set
            {
                if (documents == null)
                {
                    SetProperty(ref documents, value);
                    //documents = value;
                }
            }
        }

        private DocumentModel selectedDocument;

        public DocumentModel SelectedDocument
        {
            get { return selectedDocument; }
            set
            {
                if (selectedDocument == null)
                {
                    selectedDocument = value;
                }
            }
        }


        public DocumentOverviewViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            init();
        }

        public async void init()
        {
            Documents = await api.GetDocuments();
        }

        //Commands
        public IMvxCommand FileListCommand => new MvxCommand(FileListPage);
        public async void FileListPage()
        {
            await this.navigationService.Navigate<FileListViewModel>();
        }

        //Commands

        public MvxCommand<DocumentModel> selectedCommand;
        public System.Windows.Input.ICommand SelectedCommand
        {
            get
            {
                selectedCommand = selectedCommand ?? new MvxCommand<DocumentModel>(DoSelectItem);
                return selectedCommand;
            }
        }

        private void DoSelectItem(DocumentModel documentModel)
        {
            navigationService.Navigate<DocumentSpecificViewModel, DocumentModel>(documentModel);  
        }
    }
}
