﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySupplyApp.Core.Models
{
    public class DocumentHistory
    {
        public DateTime Timestamp;
        public string Headline;
        public string Description;

        public override string ToString()
        {
            return "time:  " + Timestamp + " : " + Description;
        }
    }
}
