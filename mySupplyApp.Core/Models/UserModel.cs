﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.ViewModels;

namespace mySupplyApp.Core.Models
{
    public class UserModel : MvxNotifyPropertyChanged
    {
        private static UserModel loggedinUser;

        public static UserModel LoggedinUser
        {
            get { return loggedinUser; }
            set
            {
                if (loggedinUser != null && value != null)
                {
                    string msg = "Another user is already logged in. Logout this user before loggin another one.";
                    //throw new InvalidOperationException(msg);
                }
                else
                {
                    loggedinUser = value;

                }
            }
        }

        private string agreementnr;
        private string username;
        private string password;

        public string Agreementnr
        {
            get { return agreementnr; }
            set { SetProperty(ref agreementnr, value); }
        }

        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

    }
}
