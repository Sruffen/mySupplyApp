﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySupplyApp.Core.ViewModels
{
    public class TestListViewModel : MvxViewModel<TestTempModel>
    {
        private readonly IMvxNavigationService navigationService;

        private static List<TestTempModel> stuffList;

        public List<TestTempModel> StuffList
        {
            get { return stuffList; }
            set
            {
                if (stuffList == null)
                {
                    stuffList = value;
                }
            }
        }

        private void StuffListAdd(TestTempModel model)
        {
            StuffList.Add(model);
            RaisePropertyChanged("StuffList");
        }

        public TestListViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            StuffList = new List<TestTempModel>();
        }

        public override void Prepare(TestTempModel parameter)
        {
            StuffListAdd(parameter);
        }

        public IMvxCommand BackCommand => new MvxCommand(Back);

        public async void Back()
        {
            await this.navigationService.Close(this);
        }

    }
}
