﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.ViewModels;

namespace mySupplyApp.Core.Models
{
    public class TestTempModel : MvxNotifyPropertyChanged
    {
        private int counter;
        private DateTime timeRecorded;
        private string name;

        public TestTempModel(int counter = 0)
        {
            this.Counter = counter;
            this.TimeRecorded = DateTime.Now;
        }

        public TestTempModel(int counter, DateTime timeRecorded)
        {
            this.Counter = counter;
            this.TimeRecorded = timeRecorded;
        }

        public int Counter
        {
            get { return counter; }
            set
            {
                counter = value;
                RaisePropertyChanged();
            }
        }

        public DateTime TimeRecorded
        {
            get { return timeRecorded; }
            set
            {
                timeRecorded = value;
                RaisePropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged();
            }
        }

        public override string ToString()
        {
            return (TimeRecorded.ToString() + "\nCounter: " + Counter);
        }
    }
}
