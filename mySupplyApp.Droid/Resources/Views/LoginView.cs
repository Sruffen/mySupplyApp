﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.ViewModels;

namespace mySupplyApp.Droid.Resources.Views
{
    [Activity(Label = "LoginView", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LoginView : MvxActivity
    {
        private IMvxInteraction<string> _interaction;
        public IMvxInteraction<string> Interaction
        {
            get => _interaction;
            set
            {
                if (_interaction != null)
                {
                    _interaction.Requested -= OnInteractionRequested;
                }
                _interaction = value;
                _interaction.Requested += OnInteractionRequested;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            mySupplyApp.Core.Helpers.DefaultPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "";

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind(this).For(view => view.Interaction).To(viewModel => viewModel.Interaction).OneWay();
            set.Apply();
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            Toast.MakeText(this, "Action selected: " + item.TitleFormatted,
                ToastLength.Short).Show();
            return base.OnOptionsItemSelected(item);
        }

        public async void OnInteractionRequested(object sender, MvxValueEventArgs<string> eventargs)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle(eventargs.Value);
            builder.SetMessage("Please fill in something correct... hackerman");
            builder.Show();
        }
    }
}