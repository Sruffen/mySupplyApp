﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using mySupplyApp.Api.Models;
using mySupplyApp.Api.Services;
using mySupplyApp.Core.Services;

/*
 * Inspiration from:
 * https://www.technical-recipes.com/2018/using-basic-authentication-in-a-web-api-application/
 */

namespace mySupplyApp.Api.Filters
{
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var authHeader = actionContext.Request.Headers.Authorization;

            if (!Constants.API_VALIDATION_ACTIVE)
                return;

            if (authHeader != null)
            {
                // Decode authorization header 
                string authenticationToken = authHeader.Parameter;
                string decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                string[] headerArray = decodedAuthenticationToken.Split(':');

                // Assign values to be used 
                string username = headerArray[0];
                string hashedkey = headerArray[1];
                string apikey = headerArray[2];

                DataAcess db = new DataAcess();

                // Check if there is access to the database.
                if (db.HasAccess())
                {
                    // Validate the APIkey, only continue if this is true.
                    if (db.ValidateAPIkey(apikey))
                    {
                        // try to get user and check if username exists.
                        User user = db.GetUserByUsername(username);
                        if (user != null)
                        {
                            // Salt the hasedkey recieved in the authorization header and compare it to the hashed value in the database.
                            string saltedKey = new Security().HashString(user.Saltvalue + hashedkey);
                            if (user.Hashvalue.Equals(saltedKey))
                            {
                                // Short circuit the method, this prevents it from returning an unwanted reponse.
                                return;
                            }
                        }
                        
                    }
                }
            }

            HandleUnathorized(actionContext);
        }

        private static void HandleUnathorized(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", "Wrong authentication string or invalid user");
        }
    }
}