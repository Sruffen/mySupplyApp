﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvvmCross.Platforms;
using mySupplyApp.Core.Models;
using MvvmCross;
using mySupplyApp.Core.ViewModels.Tabs;
using System.IO;

namespace mySupplyApp.Core.ViewModels
{
    public class DocumentSpecificViewModel : MvxViewModel<DocumentModel>
    {
        private readonly IMvxNavigationService navigationService;

        private DocumentModel model;

        public DocumentModel Model
        {
            get { return model; }
            set { SetProperty<DocumentModel>(ref model, value); }
        }

        private string path;

        public string Path
        {
            get { return path; }
            set { path = value; }
        }


        private DateTime created;
        public DateTime Created
        {
            get { return created; }
            set { SetProperty<DateTime>(ref created, value); }
        }

        public MvxViewModel<DocumentModel> detailsTab { get; set; }
        public MvxViewModel<List<DocumentHistory>> historyTab { get; set; }
        public MvxViewModel annexTab { get; set; }
        public MvxViewModel validationTab { get; set; }

        public DocumentSpecificViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
            detailsTab = Mvx.IoCProvider.IoCConstruct<DetailsTab>();
            historyTab = Mvx.IoCProvider.IoCConstruct<HistoryTab>();
            validationTab = Mvx.IoCProvider.IoCConstruct<ValidationTab>();
            annexTab = Mvx.IoCProvider.IoCConstruct<AnnexTab>();
        }

        public override void Prepare(DocumentModel document)
        {
            Model = document;
            Created = model.Created;
            detailsTab.Prepare(document);
            historyTab.Prepare(document.DocumentHistories);
        }

    }
}
