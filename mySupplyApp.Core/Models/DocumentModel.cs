﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mySupplyApp.Core.ViewModels;

namespace mySupplyApp.Core.Models
{
    public class DocumentModel
    {
        public int id { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public UserModel User { get; set; }
        public string Recipent { get; set; }
        public string Destination { get; set; }
        public bool Out { get; set; }
        public bool In { get; set; }
        private static byte[] bytes;
        public byte[] Bytes {
            get
            {
                return bytes;
            }
            set
            {
                bytes = value;
            }
        }
        public void SetBytes(byte[] bytes)
        {
            Bytes = bytes;
        }
        public List<DocumentHistory> DocumentHistories;

        public override string ToString()
        {
            return Name;
        }
    }
}
