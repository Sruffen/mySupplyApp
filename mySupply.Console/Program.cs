﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mySupplyApp.Api.Models;
using mySupplyApp.Core.Models;
using mySupplyApp.Core.Services;

namespace mySupplyApp.CommandConsole
{
    class Program
    {
        private static userT[] userTs =
        {
            new userT("9999", "Test", "test"),
            new userT("0000", "Admin", "1234"),
            new userT("1000", "Daniel", "dgl"),
            new userT("0200", "Tim", "tm"),
            new userT("0030", "Anders", "agbn")
        };

        // Change these paths to your computers paths.
        private static string[] pdfPaths =
        {
            @"C:\Users\Anders\source\repos\mySupplyApp\mySupply.Console\TestDocuments\Testpdf.pdf",
            @"C:\Users\Anders\source\repos\mySupplyApp\mySupply.Console\TestDocuments\Testpdf2.pdf",
            @"C:\Users\Anders\source\repos\mySupplyApp\mySupply.Console\TestDocuments\Testpdf3.pdf"
        };

        static void Main(string[] args)
        {
            bool done = false;

            while (!done)
            {
                Console.Write("\n\n Enter command => ");
                string command = Console.ReadLine();

                switch (command.ToLower())
                {
                    case "create":
                        CreateUser();
                        break;

                    case "list":
                        ListUsers();
                        break;

                    case "setup":
                        AddData();
                        break;

                    case "clear":
                        Console.Clear();
                        break;

                    default:
                        break;
                }
            }
        }

        private static void AddData()
        {
            if (File.Exists(pdfPaths[0]) && File.Exists(pdfPaths[1]) && File.Exists(pdfPaths[2]))
            {
                Console.WriteLine("Adding data....");
                Console.WriteLine(" -> Users");

                // Create test user

                foreach (userT item in userTs)
                {
                    string agreementnr = item.agmnt;
                    string username = item.name;
                    string password = item.pass;

                    Security sec = new Security();
                    string usernamepasshash = sec.HashString(username + password);
                    string saltvalue = sec.GetSaltValue();
                    string hashedvalue = sec.HashString(saltvalue + usernamepasshash);
                    User user = new User(agreementnr, username, hashedvalue, saltvalue);
                    createusercommand(user);
                }

                Console.WriteLine(" -> Apikeys");
                CreatApiKey("AFA27ASD", true);
                CreatApiKey("TESTKEY", false);

                Console.WriteLine(" -> Documents");
                AddDocuments();
            }
            else
            {
                Console.WriteLine("Couldnt find test docuemtns, you may need to change the paths in the command console projekt.");
                Console.ReadKey();
            }
            
        }

        private static void ListUsers()
        {
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("SELECT * FROM users ");
            command.CommandText = sqlQuery;

            // Add parameters here

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        Console.WriteLine(" Id: ".PadRight(5, ' ') + "Agmnt. Nr: ".PadRight(12, ' ') + "Username: ".PadRight(15,' ') + "Hashed value: ".PadRight(64, ' '));
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("id"));
                            string agnr = reader.GetString(reader.GetOrdinal("agreementnumber"));
                            string name = reader.GetString(reader.GetOrdinal("username"));
                            string pass = reader.GetString(reader.GetOrdinal("hashvalue"));
                            string salt = reader.GetString(reader.GetOrdinal("saltvalue"));


                            Console.WriteLine(" " + id.ToString().PadRight(5, ' ') + agnr.PadRight(12, ' ') + name.PadRight(15, ' ') + pass);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private static void CreateUser()
        {
            Console.WriteLine("\n Creating a new user:");
            Console.Write("Enter agreementnr => ");
            string agreementnr = Console.ReadLine();

            Console.Write("Enter username => ");
            string username = Console.ReadLine();

            Console.Write("Enter password => ");
            string password = Console.ReadLine();

            Security sec = new Security();

            string usernamepasshash = sec.HashString(username + password);

            string saltvalue = sec.GetSaltValue();
            string hashedvalue = sec.HashString(saltvalue + usernamepasshash);

            User user = new User(agreementnr, username, hashedvalue, saltvalue);

            createusercommand(user);
        }

        private static void createusercommand(User user) { 
       
            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("INSERT INTO users VALUES (@agreementnr, @username, @hashvalue, @saltvalue)");
            command.CommandText = sqlQuery;

            // Add parameters here
            command.Parameters.Add(new SqlParameter("agreementnr", user.Agreementnr));
            command.Parameters.Add(new SqlParameter("username", user.Username));
            command.Parameters.Add(new SqlParameter("hashvalue", user.Hashvalue));
            command.Parameters.Add(new SqlParameter("saltvalue", user.Saltvalue));

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        
                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private static void CreatApiKey(string apikeyString = "", bool active = true)
        {

            if (string.IsNullOrWhiteSpace(apikeyString))
            {
                apikeyString = Path.GetRandomFileName();
                apikeyString = apikeyString.Replace(".", ""); // Remove period.
            }

            string sqlQuery = "";
            SqlCommand command = new SqlCommand();

            // Create query here
            sqlQuery = string.Format("INSERT INTO apikeys VALUES (@key, @active)");
            command.CommandText = sqlQuery;

            // Add parameters here
            command.Parameters.Add(new SqlParameter("key", apikeyString));
            command.Parameters.Add(new SqlParameter("active", active));

            try
            {
                using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                {
                    command.Connection = connection;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        public static void AddDocuments()
        {
            Random rng = new Random();
            foreach (string path in pdfPaths)
            {
                
                string filename = Path.GetFileNameWithoutExtension(path);
                byte[] bytes = System.IO.File.ReadAllBytes(path);
                userT user = userTs[rng.Next(0, 5)];
                DocumentModel doc = new DocumentModel();

                doc.Name = filename + "-" + user.name + "-" + DateTime.Now.ToFileTimeUtc();


                int documentId = 0, returnId = 0;
                string sqlQuery = "";
                SqlCommand commandPrimus = new SqlCommand();
                SqlCommand commandSecundus = new SqlCommand();

                // Create query here
                sqlQuery = string.Format("INSERT INTO documents OUTPUT INSERTED.ID VALUES(@docname)");
                commandPrimus.CommandText = sqlQuery;

                sqlQuery = string.Format("INSERT INTO dataunits OUTPUT INSERTED.ID VALUES(@bytes, @docID)");
                commandSecundus.CommandText = sqlQuery;

                // Add parameters here
                commandPrimus.Parameters.Add(new SqlParameter("docname", doc.Name));

                commandSecundus.Parameters.Add(new SqlParameter("bytes", bytes));

                // Add condtitional info here:

                // Try to execute sql query
                try
                {
                    using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                    {

                        commandPrimus.Connection = connection;
                        commandSecundus.Connection = connection;
                        connection.Open();

                        // Excute fir command
                        documentId = (int)commandPrimus.ExecuteScalar();

                        // use the id from the first as a parameter in the second command
                        commandSecundus.Parameters.Add(new SqlParameter("docID", documentId));

                        // exeute the second command.
                        returnId = (int)commandSecundus.ExecuteScalar();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    Console.ReadKey();
                }
            }


        }
        
        private class userT
        {
            public string agmnt;
            public string name;
            public string pass;

            public userT(string agreementnr, string username, string password)
            {
                agmnt = agreementnr;
                name = username;
                pass = password;
            }
        }
    }
}
